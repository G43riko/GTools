export interface SimpleVector2 {
    x: number;
    y: number;
}
