import { SimpleVector2 } from "gtools/math";

export interface XYWH extends SimpleVector2 {
    w: number;
    h: number;
}
