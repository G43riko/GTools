import { SimpleVector2 } from "gtools/math";

export interface Ray2 {
    origin: SimpleVector2;
    direction: SimpleVector2;
}
