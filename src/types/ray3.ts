import { SimpleVector3 } from "gtools/math";

export interface Ray3 {
    origin: SimpleVector3;
    direction: SimpleVector3;
}
